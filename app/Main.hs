-- Simple SIR model in Haskell

import Data.Random
import Data.Random.Distribution.Binomial
import System.Random.MWC (create, createSystemRandom, GenIO)


-- | Represent the state of the world in our model.
data WorldState = WorldState {step :: Int,
                              susceptible :: Int,
                              infected :: Int,
                              removed :: Int} deriving (Show,Eq)


-- | Compute the total population size given a WorldState state
popsize :: WorldState -> Int
popsize state = (susceptible state) + (infected state) + (removed state)
  
  
-- | A kernel representing a state transition
kernel :: Double -> Double -> WorldState -> RVar WorldState
kernel beta gamma state = do
  infections <- binomial (susceptible state) infprob
  removals <- binomial (infected state) remprob
  
  return WorldState {step=(step state) + 1,
                     susceptible=(susceptible state) - infections,
                     infected=(infected state) + infections - removals,
                     removed=(removed state) + removals}

  where
    infprob = (1.0 :: Double) - exp(-beta * (fromIntegral $ infected state) / (fromIntegral $ popsize state))
    remprob = (1.0 :: Double) - exp(-gamma)


-- | Create the kernel function with beta=0.2, gamma=0.14
kernel' = kernel 0.2 0.14


-- | An WorldState model with `n` timesteps and
--   initial conditions `state`.  This is basically
--   a probabilistic program!
model :: Int -> WorldState -> RVar [WorldState]
model 0 state = do
  return [state]
model n state = do
  state' <- kernel' state
  states <- model (n-1) state'
  return $ state':states


-- | Simulate an outcome from `model`
simulate :: RVar [WorldState] -> IO [WorldState]
simulate model = do
  g <- createSystemRandom
  sampleFrom g $ model  -- ^ I need to understand how this works!


-- | Example initial conditions
initState :: WorldState
initState = WorldState {step=0, susceptible=999, infected=1, removed=0}


-- | Create a model with 100 timesteps and `initState` initial  conditions
main :: IO ()
main = do
  let m = model 100 initState  -- ^ m is our probabilistic program!
  sim <- simulate m
  -- How cool would to be to do `logprob m sim`?
  print sim
